package com.jose.demotest

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.PopupMenu
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.io.File


/**
 * Created by Jose on 01/02/2018.
 */
class ProfileFragment : Fragment() {

    lateinit var v:View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_profile, container, false)

        v.image.setOnClickListener {

            val popup = PopupMenu(ContextThemeWrapper(v.context, R.style.PopupMenuOverlapAnchor), v.image)

            popup.menuInflater.inflate(R.menu.popup_menu_profle, popup.menu)

            popup.setOnMenuItemClickListener { item ->

                if(item.itemId == R.id.camera) {

                    ImagePicker.cameraOnly()
                        .start(activity)


                }

                if(item.itemId == R.id.gallery) {

                    ImagePicker.create(this)
                            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                            .folderMode(true) // folder mode (false by default)
                            .toolbarFolderTitle("Carpeta") // folder selection title
                            .toolbarImageTitle("Toque para seleccionar") // image selection title
                            .showCamera(false)
                            .single() // single mode
                            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                            .start()

                }

                if(item.itemId == R.id.both) {
                    ImagePicker.create(this)
                            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                            .folderMode(true) // folder mode (false by default)
                            .toolbarFolderTitle("Carpeta") // folder selection title
                            .toolbarImageTitle("Toque para seleccionar") // image selection title
                            .single() // single mode
                            .showCamera(true) // show camera or not (true by default)
                            .start()
                }

                return@setOnMenuItemClickListener true
            }

            popup.show()

        }

        return v
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            val image = ImagePicker.getFirstImageOrNull(data)

            image?.let {
                Glide.with(context!!)
                        .load(File(it.path))
                        .apply(RequestOptions.circleCropTransform())
                        .into(view!!.image);
            }

        }
    }
}