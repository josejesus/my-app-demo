package com.jose.demotest

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.jose.demotest.ask.AskFragment
import com.jose.demotest.services.ServicesFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var fragment: Fragment = AskFragment()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_ask -> {
                fragment = AskFragment()
                toolbar_title.text = "Perfil de enfermera"
            }
            R.id.navigation_diary -> {
                fragment = DiaryFragment()
                toolbar_title.text = "Eventos"
            }
            R.id.navigation_services -> {
                fragment = ServicesFragment()
                toolbar_title.text = "Mis Servicios"
            }
            R.id.navigation_profile -> {
                fragment = ProfileFragment()
                toolbar_title.text = "Perfil"

            }
        }

        supportFragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();
        toolbar_title.anim(R.anim.toolbar_title_change)

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        with (supportActionBar!!) {
            //setHomeButtonEnabled(true);
            //setDisplayHomeAsUpEnabled(true)
        }
        supportFragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit();

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fragment.onActivityResult(requestCode, resultCode, data)
    }
}
