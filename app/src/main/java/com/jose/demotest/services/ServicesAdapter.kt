package com.jose.demotest.services

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jose.demotest.R
import com.jose.demotest.inflate
import kotlinx.android.synthetic.main.row_service.view.*

/**
 * Created by Jose on 01/02/2018.
 */
class ServicesAdapter(val services:List<Service>): RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(services[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_service))

    override fun getItemCount() = services.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(service: Service) = with(itemView){

            Glide.with(context!!).load(service.image).apply(RequestOptions.circleCropTransform()).into(image)

            name.text = service.name
            time.text = service.time
            addres.text = service.address
            timeAgo.text = service.time

        }

    }
}