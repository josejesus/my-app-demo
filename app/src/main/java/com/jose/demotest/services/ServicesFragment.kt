package com.jose.demotest.services

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jose.demotest.R
import com.jose.demotest.init
import kotlinx.android.synthetic.main.fragment_services.view.*

/**
 * Created by Jose on 01/02/2018.
 */
class ServicesFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_services, container, false)

    }

    override fun onStart() {
        super.onStart()

        val services = ArrayList<Service>()

        services.add(Service("Juana Palacios","123 Wall Street - Chicago", R.drawable.image, "5 horas", "Hace 10 dias", "completado"))
        services.add(Service("Zana Martinez", "789 Gear Street - New York", R.drawable.image2,"2 horas", "Hace 44 dias", "completado"))
        services.add(Service("Alessa Karts", "876 Diamong Street - Tenesse", R.drawable.image3,"1 horas", "Hace 7 dias", "cancelados"))
        services.add(Service("Diego Lopes", "543 Ruby Street - Google", R.drawable.image4,"4 horas", "Hace 33 dias", "completado"))
        services.add(Service("Juana Palacios", "219 PHP Street - IBM", R.drawable.image5,"5 horas", "Hace 10 dias", "cancelados"))
        services.add(Service("Diana Diaz", "643 Python Street - Apple", R.drawable.image6,"15 horas", "Hace 2 dias", "completado"))
        services.add(Service("Zana Martinez", "233 Google Street - Pirate", R.drawable.image7,"2 horas", "Hace 44 dias", "cancelados"))
        services.add(Service("Alessa Karts", "888 Main Street - Canada", R.drawable.image8,"1 horas", "Hace 7 dias", "completado"))
        services.add(Service("Diego Lopes", "143 Mexico Street - Cancun", R.drawable.image9,"4 horas", "Hace 33 dias", "cancelados"))

        view!!.rvServices.init(ServicesAdapter(services))

        view!!.all.setOnClickListener {
            view!!.rvServices.init(ServicesAdapter(services))
        }

        view!!.completed.setOnClickListener {
            var filter = services.filter { service -> service.status == "completado" }
            view!!.rvServices.init(ServicesAdapter(filter))
        }

        view!!.canceled.setOnClickListener {
            var filter = services.filter { service -> service.status == "cancelados" }
            view!!.rvServices.init(ServicesAdapter(filter))
        }


    }
}