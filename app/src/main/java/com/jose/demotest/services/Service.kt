package com.jose.demotest.services

/**
 * Created by Jose on 01/02/2018.
 */
data class Service(
        val name:String,
        val address:String,
        val image:Int,
        val time:String,
        val timeAgo:String,
        val status:String
)