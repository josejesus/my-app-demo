package com.jose.demotest.ask

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.jose.demotest.R
import com.jose.demotest.ViewPagerAdapter
import com.jose.demotest.init
import kotlinx.android.synthetic.main.fragment_ask.view.*

/**
 * Created by Jose on 01/02/2018.
 */
class AskFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_ask, container, false)
    }

    override fun onStart() {
        super.onStart()

        val hours = ArrayList<String>()

        for (i in 6..12) hours.add("$i:00 AM")

        view!!.rvHours.init(HoursAdapter(hours), LinearLayout.HORIZONTAL)

        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(InformationAskFragment(), "Información")
        adapter.addFragment(ReviewsAskFragment(), "Reseñas de clientes")
        view!!.viewpager.adapter = adapter

        view!!.tabs.setupWithViewPager(view!!.viewpager)

    }

}