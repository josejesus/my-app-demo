package com.jose.demotest.ask

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.jose.demotest.R
import com.jose.demotest.inflate
import kotlinx.android.synthetic.main.row_ask_hour.view.*

/**
 * Created by Jose on 01/02/2018.
 */
class HoursAdapter(val hours:List<String>): RecyclerView.Adapter<HoursAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(hours[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_ask_hour))

    override fun getItemCount() = hours.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(hour: String) = with(itemView){

            tv_hour.text = hour

        }

    }
}