package com.jose.demotest.ask

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jose.demotest.R

/**
 * Created by Jose on 01/02/2018.
 */
class ReviewsAskFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reviews_ask, container, false)
    }
}