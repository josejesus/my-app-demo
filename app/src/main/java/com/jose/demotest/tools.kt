package com.jose.demotest

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.LinearLayout

/**
 * Created by Jose on 01/02/2018.
 */

/*
* Extension for adapters of recyclers view
* */
fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

/*Extension for set properties to recycler view */
fun RecyclerView.init(adapter:RecyclerView.Adapter<*>, orientation:Int = LinearLayout.VERTICAL) {

    this.isNestedScrollingEnabled = false
    this.layoutManager = LinearLayoutManager(context, orientation, false)
    this.adapter = adapter

}

/* fun for animate fast view*/
fun View.anim(anim:Int) = this.startAnimation(AnimationUtils.loadAnimation(context, anim))

