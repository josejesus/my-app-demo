## TestApp

A simple test app.

## Screenshots

<p float="left">

    <img src="https://gitlab.com/josejesus/my-app-demo/raw/9620b50bf30648c74b0fa14d5a7df18838972e21/screenshots/Screenshot_1.png" height="640" width="360"/>
    
    <img src="https://gitlab.com/josejesus/my-app-demo/raw/9620b50bf30648c74b0fa14d5a7df18838972e21/screenshots/Screenshot_2.png" height="640" width="360"/>
    
    <img src="https://gitlab.com/josejesus/my-app-demo/raw/9620b50bf30648c74b0fa14d5a7df18838972e21/screenshots/Screenshot_3.png" height="640" width="360"/>
    
    <img src="https://gitlab.com/josejesus/my-app-demo/raw/9620b50bf30648c74b0fa14d5a7df18838972e21/screenshots/Screenshot_4.png" height="640" width="360"/>
    
</p>